package me.zhengken.lyricview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.VelocityDetector;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.text.SimpleTextLayout;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;
import org.mozilla.universalchardet.UniversalDetector;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class LyricView extends Component implements Component.TouchEventListener, Component.DrawTask, Component.EstimateSizeListener {
    HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"LyricView");

    public static final int LEFT = 0;
    public static final int CENTER = 1;
    public static final int RIGHT = 2;

    private static final float SLIDE_COEFFICIENT = 0.2f;

    private static final int UNITS_SECOND = 1000;
    private static final int UNITS_MILLISECOND = 1;

    private static final int FLING_ANIMATOR_DURATION = 500 * UNITS_MILLISECOND;

    private static final int THRESHOLD_Y_VELOCITY = 1600;

    private static final int INDICATOR_ICON_PLAY_MARGIN_LEFT = 7;
    private static final int INDICATOR_ICON_PLAY_WIDTH = 15;
    private static final int INDICATOR_LINE_MARGIN = 10;
    private static final int INDICATOR_TIME_TEXT_SIZE = 10;
    private static final int INDICATOR_TIME_MARGIN_RIGHT = 7;

    private static final int DEFAULT_TEXT_SIZE = 16;
    private static final int DEFAULT_MAX_LENGTH = 300;
    private static final int DEFAULT_LINE_SPACE = 25;

    private int mHintColor;
    private int mDefaultColor;
    private int mHighLightColor;
    private int mTextAlign;


    private int mLineCount;
    private int mTextSize;
    private float mLineHeight;
    private LyricInfo mLyricInfo;
    private String mDefaultHint;
    private int mMaxLength;

    private Paint mTextPaint;
    private Paint mBtnPlayPaint;
    private Paint mLinePaint;
    private Paint mTimerPaint;

    private boolean mFling = false;
    private AnimatorValue mFlingAnimator;
    private float mScrollY = 0;
    private float mLineSpace = 0;
    private boolean mIsShade;
    private float mShaderWidth = 0;
    private int mCurrentPlayLine = 0;
    private boolean mShowIndicator;

    private VelocityDetector mVelocityTracker;
    private float mVelocity = 0;
    private float mDownX;
    private float mDownY;
    private float mLastScrollY;
    private boolean mUserTouch = false;
    private int mLineNumberUnderIndicator = 0;
    private Rect mBtnPlayRect = new Rect();
    private Rect mTimerRect;
    private String mDefaultTime = "00:00";

    private int mLineColor = Color.getIntColor("#EFEFEF");
    private int mBtnColor = Color.getIntColor("#EFEFEF");

    private List<Integer> mLineFeedRecord = new ArrayList<>();
    private boolean mEnableLineFeed = false;
    private int mExtraHeight = 0;

    private int mTextHeight;

    private String mCurrentLyricFilePath = null;

    private OnPlayerClickListener mClickListener;

    private static final int MSG_HIDE_INDICATOR = 0;

    public LyricView(Context context) {
        super(context);
        addDrawTask(this);
        setTouchEventListener(this);
        initMyView();
    }

    public LyricView(Context context, AttrSet attributeSet) {
        super(context, attributeSet);
        getAttrs(attributeSet);
        initMyView();
        setTouchEventListener(this);
        addDrawTask(this);
    }

    public LyricView(Context context, AttrSet attributeSet, int i) {
        super(context, attributeSet, i);
        getAttrs(attributeSet);
        initMyView();
        setTouchEventListener(this);
        addDrawTask(this);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {

        mBtnPlayRect.set(INDICATOR_ICON_PLAY_MARGIN_LEFT,
                (int) (getHeight() * 0.5f - AttrHelper.vp2px(INDICATOR_ICON_PLAY_WIDTH,getContext()) * 0.5f),
                (int) (AttrHelper.vp2px(INDICATOR_ICON_PLAY_WIDTH,getContext()) + AttrHelper.vp2px(INDICATOR_ICON_PLAY_MARGIN_LEFT,getContext())),
                (int) (getHeight() * 0.5f + AttrHelper.vp2px(INDICATOR_ICON_PLAY_WIDTH,getContext()) * 0.5f));
        mShaderWidth = getWidth() * 0.4f;
        if (scrollable()) {
            if (mShowIndicator) {
                drawIndicator(canvas);
            }
            for (int i = 0; i < mLineCount; i++) {
                float x = 0;
                switch (mTextAlign) {
                    case LEFT:
                        x = INDICATOR_ICON_PLAY_MARGIN_LEFT + INDICATOR_LINE_MARGIN + mBtnPlayRect.getWidth();
                        break;
                    case CENTER:
                        x = getWidth() * 0.5f;
                        break;
                    case RIGHT:
                        x = getWidth() - INDICATOR_LINE_MARGIN * 2 - mTimerRect.getWidth() - INDICATOR_ICON_PLAY_MARGIN_LEFT;
                        break;
                }

                float y;
                if (mEnableLineFeed && i > 0) {
                    y = getWidth() * 0.5f + i * mLineHeight - mScrollY + mLineFeedRecord.get(i - 1);
                } else {
                    y = getHeight() * 0.5f + i * mLineHeight - mScrollY;
                }
                if (y < 0) {
                    continue;
                }
                if (y > getHeight()) {
                    break;
                }
                if (i == mCurrentPlayLine - 1) {
                    mTextPaint.setColor(new Color(mHighLightColor));
                } else if (i == mLineNumberUnderIndicator && mShowIndicator) {
                    mTextPaint.setColor(Color.LTGRAY);
                } else {
                    mTextPaint.setColor(new Color(mDefaultColor));
                }
                if (mIsShade && (y > getHeight() - mShaderWidth || y < mShaderWidth)) {
                    if (y < mShaderWidth) {
                        mTextPaint.setAlpha(y / mShaderWidth);
                    } else {
                        mTextPaint.setAlpha((getHeight() - mShaderWidth)/ y);
                    }
                }
                if (mEnableLineFeed) {
                    SimpleTextLayout simpleTextLayout = new SimpleTextLayout(mLyricInfo.songLines.get(i).content,mTextPaint,new Rect(0,0,mMaxLength,mTextHeight),mMaxLength);
                    int linecout = simpleTextLayout.getLineCount();
                    if (linecout > 1){
                        simpleTextLayout = new SimpleTextLayout(mLyricInfo.songLines.get(i).content,mTextPaint,new Rect(0,0,mMaxLength,mTextHeight * linecout),mMaxLength);
                    }
                    canvas.save();
                    canvas.translate(x, y);
                    simpleTextLayout.drawText(canvas);
                    canvas.restore();
                }else {
                    canvas.drawText(mTextPaint, mLyricInfo.songLines.get(i).content, x, y);
                }
            }
        } else {
            mTextPaint.setColor(new Color(mHintColor));
            canvas.drawText( mTextPaint,mDefaultHint, getWidth() / 2.0f, getHeight() / 2.0f);
        }
    }

    private void getAttrs(AttrSet attrs) {
        if (attrs.getAttr("fadeInFadeOut").isPresent()){
            mIsShade = attrs.getAttr("fadeInFadeOut").get().getBoolValue();
        }else {
            mIsShade=false;
        }
        if (attrs.getAttr("hint").isPresent()){
            mDefaultHint = attrs.getAttr("hint").get().getStringValue();
        }else {
            mDefaultHint="No Lyrics";
        }
        if (attrs.getAttr("hintColor").isPresent()){
            mHintColor = attrs.getAttr("hintColor").get().getColorValue().getValue();
        }else {
            mHintColor=0XFFFFFFFF;
        }
        if (attrs.getAttr("textColor").isPresent()){
            mDefaultColor = attrs.getAttr("textColor").get().getColorValue().getValue();
        }else {
            mDefaultColor=0xFFFFFF00;
        }
        if (attrs.getAttr("highlightColor").isPresent()){
            mHighLightColor = attrs.getAttr("highlightColor").get().getColorValue().getValue();
        }else {
            mHighLightColor=0xFFFF0000;
        }
        if (attrs.getAttr("textSize").isPresent()){
            mTextSize = attrs.getAttr("textSize").get().getDimensionValue();
        }else {
            mTextSize=AttrHelper.fp2px(DEFAULT_TEXT_SIZE,getContext());
        }
        if (attrs.getAttr("textAlign").isPresent()){
            mTextAlign = attrs.getAttr("textAlign").get().getIntegerValue();
        }else {
            mTextAlign=1;
        }
        if (attrs.getAttr("maxLength").isPresent()){
            mMaxLength = attrs.getAttr("maxLength").get().getDimensionValue();
        }else {
            mMaxLength=AttrHelper.vp2px(DEFAULT_MAX_LENGTH,getContext());
        }
        if (attrs.getAttr("lineSpace").isPresent()){
            mLineSpace = attrs.getAttr("lineSpace").get().getDimensionValue();
        }else {
            mLineSpace = AttrHelper.vp2px(DEFAULT_LINE_SPACE,getContext());
        }
    }

    public void setOnPlayerClickListener(OnPlayerClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public void setAlignment(int alignment) {
        mTextAlign = alignment;
    }

    public void setCurrentTimeMillis(long current) {
        scrollToCurrentTimeMillis(current);
    }

    public  void setLyrcFileInputStream(InputStream inputStream){
        setLyricFileFileInputStream(inputStream, "UTF-8");
    }
    public void setLyricFileFileInputStream(InputStream inputStream, String charsetName) {
        if (inputStream != null) {
            setupLyricResource(inputStream, charsetName);
            for (int i = 0; i < mLyricInfo.songLines.size(); i++) {
                SimpleTextLayout simpleTextLayout = new SimpleTextLayout(mLyricInfo.songLines.get(i).content,mTextPaint,new Rect(),mMaxLength);
                if (simpleTextLayout.getLineCount() > 1) {
                    mEnableLineFeed = true;
                    mExtraHeight = mExtraHeight + (simpleTextLayout.getLineCount() - 1) * mTextHeight;
                }
                mLineFeedRecord.add(i, mExtraHeight);
            }

        } else {
            invalidateView();
        }
    }

    public void setLyricFile(File file) {

        if (file == null || !file.exists()) {
            reset();
            mCurrentLyricFilePath = "";
            return;
        } else if (file.getPath().equals(mCurrentLyricFilePath)) {
            return;
        } else {
            mCurrentLyricFilePath = file.getPath();
            reset();
        }
        FileInputStream fis = null;
        try {

            fis = new FileInputStream(file);
            byte[] buf = new byte[1024];
            UniversalDetector detector = new UniversalDetector(null);
            int nread;
            while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }

            detector.dataEnd();
            String encoding = detector.getDetectedCharset();
            if (encoding != null) {
                setLyricFile(file, encoding);
            } else {
                setLyricFile(file, "UTF-8");
            }
            detector.reset();

        } catch (IOException e) {
            HiLog.error(logLabel,"setLyricFile() IOException %{public}s",e.getMessage());
        }finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    HiLog.error(logLabel,"setLyricFile() IOException %{public}s",e.getMessage());
                }
            }
        }
    }

    public void setLyricFile(File file, String charsetName) {
        if (file != null && file.exists()) {
            try {
                setupLyricResource(new FileInputStream(file), charsetName);
                for (int i = 0; i < mLyricInfo.songLines.size(); i++) {
                    SimpleTextLayout simpleTextLayout = new SimpleTextLayout(mLyricInfo.songLines.get(i).content,mTextPaint,new Rect(),AttrHelper.vp2px(DEFAULT_MAX_LENGTH,getContext()));
                    if (simpleTextLayout.getLineCount() > 1) {
                        mEnableLineFeed = true;
                        mExtraHeight = mExtraHeight + (simpleTextLayout.getLineCount() - 1) * mTextHeight;
                    }
                    mLineFeedRecord.add(i, mExtraHeight);
                }
            } catch (FileNotFoundException e) {
                HiLog.error(logLabel,"setLyricFile(File file, String charsetName) IOException %{public}s",e.getMessage());
            }
        } else {
            invalidateView();
        }
    }

    private void setLineSpace(float lineSpace) {
        if (mLineSpace != lineSpace) {
            mLineSpace = AttrHelper.vp2px(lineSpace,getContext());
            measureLineHeight();
            mScrollY = measureCurrentScrollY(mCurrentPlayLine);
            invalidateView();
        }
    }

    public void reset() {
        resetView();
    }

    private void actionCancel() {
        releaseVelocityTracker();
    }

    private void actionDown(TouchEvent event) {
        handler.removeEvent(MSG_HIDE_INDICATOR);
        mLastScrollY = mScrollY;
        mDownX = event.getPointerPosition(0).getX();
        mDownY = event.getPointerPosition(0).getY();
        if (mFlingAnimator != null) {
            mFlingAnimator.cancel();
            mFlingAnimator = null;
        }
        setUserTouch(true);
    }

    private boolean overScrolled() {

        return scrollable() && (mScrollY > mLineHeight * (mLineCount - 1) + mLineFeedRecord.get(mLineCount - 1) + (mEnableLineFeed ? mTextHeight : 0) || mScrollY < 0);
    }


    private void actionMove(TouchEvent event) {
        if (scrollable()) {
            final VelocityDetector tracker = mVelocityTracker;
            tracker.calculateCurrentVelocity(UNITS_SECOND);
            mScrollY = mLastScrollY + mDownY - event.getPointerPosition(0).getY();
            mVelocity = tracker.getVerticalVelocity();
            measureCurrentLine();
        }
    }

    private void actionUp(TouchEvent event) {
        handler.sendEvent(MSG_HIDE_INDICATOR,3 * UNITS_SECOND);
        releaseVelocityTracker();

        if (scrollable()) {
            if (overScrolled() && mScrollY < 0) {
                smoothScrollTo(0);
                return;
            }
            if (overScrolled() && mScrollY > mLineHeight * (mLineCount - 1) + mLineFeedRecord.get(mLineCount - 1) + (mEnableLineFeed ? mTextHeight : 0)) {
                smoothScrollTo(mLineHeight * (mLineCount - 1) + mLineFeedRecord.get(mLineCount - 1) + (mEnableLineFeed ? mTextHeight : 0));
                return;
            }
            if (Math.abs(mVelocity) > THRESHOLD_Y_VELOCITY) {
                doFlingAnimator(mVelocity);
                return;
            }
            if (mShowIndicator && clickPlayer(event)) {
                if (mLineNumberUnderIndicator != mCurrentPlayLine) {
                    mShowIndicator = false;
                    if (mClickListener != null) {
                        setUserTouch(false);
                        mClickListener.onPlayerClicked(mLyricInfo.songLines.get(mLineNumberUnderIndicator).start, mLyricInfo.songLines.get(mLineNumberUnderIndicator).content);
                    }
                }
            }
        }
    }

    private String measureCurrentTime() {
        DecimalFormat format = new DecimalFormat("00");
        if (mLyricInfo != null && mLineCount > 0 && mLineNumberUnderIndicator - 1 < mLineCount && mLineNumberUnderIndicator > 0) {
            return format.format(mLyricInfo.songLines.get(mLineNumberUnderIndicator - 1).start / 1000 / 60) + ":" + format.format(mLyricInfo.songLines.get(mLineNumberUnderIndicator - 1).start / 1000 % 60);
        }
        if (mLyricInfo != null && mLineCount > 0 && (mLineNumberUnderIndicator - 1) >= mLineCount) {
            return format.format(mLyricInfo.songLines.get(mLineCount - 1).start / 1000 / 60) + ":" + format.format(mLyricInfo.songLines.get(mLineCount - 1).start / 1000 % 60);
        }
        if (mLyricInfo != null && mLineCount > 0 && mLineNumberUnderIndicator - 1 <= 0) {
            return format.format(mLyricInfo.songLines.get(0).start / 1000 / 60) + ":" + format.format(mLyricInfo.songLines.get(0).start / 1000 % 60);
        }
        return mDefaultTime;
    }

    private void drawIndicator(Canvas canvas) {

        //绘制 播放 按钮
        Path pathPlay = new Path();
        float yCoordinate = mBtnPlayRect.left + (float) Math.sqrt(Math.pow(mBtnPlayRect.getWidth(), 2) - Math.pow(mBtnPlayRect.getWidth() * 0.5f, 2));
        float remainWidth = mBtnPlayRect.right - yCoordinate;

        float xCoor = mBtnPlayRect.getCenterX() - mBtnPlayRect.getWidth() * 0.5f;
        float xYcoor = mBtnPlayRect.getCenterY() - mBtnPlayRect.getHeight() * 0.5f;
        pathPlay.moveTo(xCoor, xYcoor);

        xCoor = mBtnPlayRect.getCenterX() - mBtnPlayRect.getWidth() * 0.5f;
        xYcoor = mBtnPlayRect.getCenterY() + mBtnPlayRect.getHeight() * 0.5f;
        pathPlay.lineTo(xCoor, xYcoor);
        pathPlay.lineTo(yCoordinate, mBtnPlayRect.getCenterY());

        xCoor = mBtnPlayRect.getCenterX() - mBtnPlayRect.getWidth() * 0.5f;
        xYcoor = mBtnPlayRect.getCenterY() - mBtnPlayRect.getHeight() * 0.5f;
        pathPlay.lineTo(xCoor, xYcoor);


        canvas.drawPath(pathPlay, mBtnPlayPaint);

        //绘制 分割线
        Path pathLine = new Path();
        pathLine.moveTo(mBtnPlayRect.right + AttrHelper.vp2px(INDICATOR_LINE_MARGIN,getContext()) - remainWidth, getHeight() * 0.5f);
        pathLine.lineTo(getWidth() - mTimerRect.getWidth() - AttrHelper.vp2px(INDICATOR_TIME_MARGIN_RIGHT,getContext()) - AttrHelper.vp2px(INDICATOR_LINE_MARGIN,getContext()), getHeight() * 0.5f);
        canvas.drawPath(pathLine, mLinePaint);

        //绘制 时间
        canvas.drawText(mTimerPaint,measureCurrentTime(), getWidth() - AttrHelper.vp2px(INDICATOR_TIME_MARGIN_RIGHT,getContext()), (getHeight() + mTimerRect.getHeight()) * 0.5f);
    }

    private boolean clickPlayer(TouchEvent event) {

        if (mBtnPlayRect != null && mDownX > (mBtnPlayRect.left - INDICATOR_ICON_PLAY_MARGIN_LEFT) && mDownX < (mBtnPlayRect.right + INDICATOR_ICON_PLAY_MARGIN_LEFT) && mDownY > (mBtnPlayRect.top - INDICATOR_ICON_PLAY_MARGIN_LEFT) && mDownY < (mBtnPlayRect.bottom + INDICATOR_ICON_PLAY_MARGIN_LEFT)) {
            float upX = event.getPointerPosition(0).getX();
            float upY = event.getPointerPosition(0).getY();
            return   upX > (mBtnPlayRect.left - INDICATOR_ICON_PLAY_MARGIN_LEFT) && upX < (mBtnPlayRect.right + INDICATOR_ICON_PLAY_MARGIN_LEFT) && upY > (mBtnPlayRect.top - INDICATOR_ICON_PLAY_MARGIN_LEFT) && upY < (mBtnPlayRect.bottom + INDICATOR_ICON_PLAY_MARGIN_LEFT);
        }
        return false;
    }

    private void doFlingAnimator(float velocity) {

        float distance = (velocity / Math.abs(velocity) * (Math.abs(velocity) * SLIDE_COEFFICIENT));
        float to = Math.min(Math.max(0, (mScrollY - distance)), (mLineCount - 1) * mLineHeight + mLineFeedRecord.get(mLineCount - 1) + (mEnableLineFeed ? mTextHeight : 0));
        mFlingAnimator = new AnimatorValue();

        mFlingAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                mScrollY = (to - mScrollY)*v;
                measureCurrentLine();
                invalidateView();
            }
        });

        mFlingAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mVelocity = 0;
                mFling = true;
            }
            @Override
            public void onStop(Animator animator) {}
            @Override
            public void onCancel(Animator animator) {}
            @Override
            public void onEnd(Animator animator) {
                mFling = false;
            }
            @Override
            public void onPause(Animator animator) {}
            @Override
            public void onResume(Animator animator) {}
        });

        mFlingAnimator.setDuration(FLING_ANIMATOR_DURATION);
        mFlingAnimator.start();
    }

    private void setUserTouch(boolean isUserTouch) {
        if (isUserTouch) {
            mUserTouch = true;
            mShowIndicator = true;
        } else {
            mUserTouch = false;
            mShowIndicator = false;
        }
    }

    private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
            mVelocityTracker.clear();
            mVelocityTracker = null;
        }
    }

    private void initMyView() {
        initPaint();
        initAllBounds();
    }

    private void initAllBounds() {

        setRawTextSize(mTextSize);

        setLineSpace(mLineSpace);
        measureLineHeight();

        mTimerRect = new Rect();
    }

    private void initPaint() {
        mTextPaint = new Paint();
        mTextPaint.setDither(true);
        mTextPaint.setAntiAlias(true);
        switch (mTextAlign) {
            case LEFT:
                mTextPaint.setTextAlign(TextAlignment.LEFT);
                break;
            case CENTER:
                mTextPaint.setTextAlign(TextAlignment.CENTER);
                break;
            case RIGHT:
                mTextPaint.setTextAlign(TextAlignment.RIGHT);
                break;
        }

        mBtnPlayPaint = new Paint();
        mBtnPlayPaint.setDither(true);
        mBtnPlayPaint.setAntiAlias(true);
        mBtnPlayPaint.setColor(new Color(mBtnColor));
        mBtnPlayPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        mBtnPlayPaint.setAlpha(128);

        mLinePaint = new Paint();
        mLinePaint.setDither(true);
        mLinePaint.setAntiAlias(true);
        mLinePaint.setColor(new Color(mLineColor));
        mLinePaint.setAlpha(64);
        mLinePaint.setStrokeWidth(1.0f);
        mLinePaint.setStyle(Paint.Style.STROKE_STYLE);

        mTimerPaint = new Paint();
        mTimerPaint.setDither(true);
        mTimerPaint.setAntiAlias(true);
        mTimerPaint.setColor(Color.WHITE);
        mTimerPaint.setTextAlign(TextAlignment.RIGHT);
        mTimerPaint.setTextSize(AttrHelper.fp2px(INDICATOR_TIME_TEXT_SIZE,getContext()));


    }

    private float measureCurrentScrollY(int line) {
        if (mEnableLineFeed && line > 1) {
            return (line - 1) * mLineHeight + mLineFeedRecord.get(line - 1);
        }
        return (line - 1) * mLineHeight;
    }

    private void invalidateView() {
        invalidate();
    }

    private void measureLineHeight() {
        Rect lineBound;
        lineBound= mTextPaint.getTextBounds(mDefaultHint);
        mTextHeight = lineBound.getHeight();
        mLineHeight = mTextHeight + mLineSpace;
    }

    /**
     * To measure current showing line number based on the view's scroll Y
     */
    private void measureCurrentLine() {
        float baseScrollY = mScrollY + mLineHeight * 0.5f;

        if (mEnableLineFeed) {
            for (int i = mLyricInfo.songLines.size(); i >= 0; i--) {
                if (baseScrollY > measureCurrentScrollY(i) + mLineSpace * 0.2) {
                    mLineNumberUnderIndicator = i - 1;
                    break;
                }
            }
        } else {
            mLineNumberUnderIndicator = (int) (baseScrollY / mLineHeight);
        }


    }

    private void smoothScrollTo(float toY) {
        final  AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                mScrollY += (toY-mScrollY)*v;

                invalidateView();
            }
        });
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mFling = true;
            }
            @Override
            public void onStop(Animator animator) { }
            @Override
            public void onCancel(Animator animator) {}
            @Override
            public void onEnd(Animator animator) {
                mFling = false;
                measureCurrentLine();
                invalidateView();
            }
            @Override
            public void onPause(Animator animator) {}

            @Override
            public void onResume(Animator animator) {}
        });
        animatorValue.setDuration(640);
        animatorValue.start();
    }

    private boolean scrollable() {
        return mLyricInfo != null && mLyricInfo.songLines != null && mLyricInfo.songLines.size() > 0;
    }

    private void scrollToCurrentTimeMillis(long time) {
        int position = 0;
        if (scrollable()) {
            for (int i = 0, size = mLineCount; i < size; i++) {
                LineInfo lineInfo = mLyricInfo.songLines.get(i);
                if (lineInfo != null && lineInfo.start >= time) {
                    position = i;
                    break;
                }
                if (i == mLineCount - 1) {
                    position = mLineCount;
                }
            }
        }
        if (mCurrentPlayLine != position) {
            mCurrentPlayLine = position;
            if (!mFling && !mUserTouch) {
                smoothScrollTo(measureCurrentScrollY(position));
            }
        }
    }

    private void setupLyricResource(InputStream inputStream, String charsetName) {
        if (inputStream != null) {
            InputStreamReader inputStreamReader = null;
            BufferedReader reader = null;
            try {
                LyricInfo lyricInfo = new LyricInfo();
                lyricInfo.songLines = new ArrayList<>();
                inputStreamReader = new InputStreamReader(inputStream,charsetName);
                reader = new BufferedReader(inputStreamReader);
                String line;
                while ((line = reader.readLine()) != null) {
                    analyzeLyric(lyricInfo, line);
                }

                mLyricInfo = lyricInfo;
                mLineCount = mLyricInfo.songLines.size();
                invalidateView();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (inputStreamReader != null){
                    try {
                        inputStreamReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            invalidateView();
        }
    }

    /**
     * 逐行解析歌词内容
     */
    private void analyzeLyric(LyricInfo lyricInfo, String line) {
        int index = line.lastIndexOf("]");
        if (line.startsWith("[offset:")) {
            // time offset
            lyricInfo.songOffset = Long.parseLong(line.substring(8, index).trim());
            return;
        }
        if (line.startsWith("[ti:")) {
            // title
            lyricInfo.songTitle = line.substring(4, index).trim();
            return;
        }
        if (line.startsWith("[ar:")) {
            // artist
            lyricInfo.songArtist = line.substring(4, index).trim();
            return;
        }
        if (line.startsWith("[al:")) {
            // album
            lyricInfo.songAlbum = line.substring(4, index).trim();
            return;
        }
        if (line.startsWith("[by:")) {
            return;
        }
        if (index >= 9 && line.trim().length() > index + 1) {
            // lyrics
            LineInfo lineInfo = new LineInfo();
            lineInfo.content = line.substring(10, line.length());
            lineInfo.start = measureStartTimeMillis(line.substring(0, index));
            lyricInfo.songLines.add(lineInfo);
        }
    }

    /**
     * 从字符串中获得时间值
     */
    private long measureStartTimeMillis(String str) {
        long minute = Long.parseLong(str.substring(1, 3));
        long second = Long.parseLong(str.substring(4, 6));
        long millisecond = Long.parseLong(str.substring(7, 9));
        return millisecond + second * 1000 + minute * 60 * 1000;
    }

    private void resetLyricInfo() {
        if (mLyricInfo != null) {
            if (mLyricInfo.songLines != null) {
                mLyricInfo.songLines.clear();
                mLyricInfo.songLines = null;
            }
            mLyricInfo = null;
        }
    }

    private void resetView() {
        mCurrentPlayLine = 0;
        resetLyricInfo();
        invalidateView();
        mLineCount = 0;
        mScrollY = 0;
        mEnableLineFeed = false;
        mLineFeedRecord.clear();
        mExtraHeight = 0;
    }

    private void setRawTextSize(float size) {
        if (size != mTextPaint.getTextSize()) {
            mTextPaint.setTextSize((int)size);
            measureLineHeight();
            mScrollY = measureCurrentScrollY(mCurrentPlayLine);
            invalidateView();
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityDetector.obtainInstance();
        }
        mVelocityTracker.addEvent(touchEvent);
        switch (touchEvent.getAction()) {
            case TouchEvent.CANCEL:
                actionCancel();
                break;
            case TouchEvent.PRIMARY_POINT_DOWN:
                actionDown(touchEvent);
                break;
            case TouchEvent.POINT_MOVE:
                actionMove(touchEvent);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                actionUp(touchEvent);
                break;
            default:
                break;
        }
        invalidateView();
        return true;
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        int width = Component.EstimateSpec.getSize(i);
        int height = Component.EstimateSpec.getSize(i1);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));
        invalidate();
        return true;
    }

    private class LyricInfo {
        List<LineInfo> songLines;

        String songArtist;
        String songTitle;
        String songAlbum;

        long songOffset;
    }

    private class LineInfo {
        String content;
        long start;
    }

    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case MSG_HIDE_INDICATOR:
                    setUserTouch(false);
                    invalidateView();
                    break;
            }
        }
    };

    public interface OnPlayerClickListener {
        void onPlayerClicked(long progress, String content);
    }
}
