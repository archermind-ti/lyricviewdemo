package me.zhengken.zkmusicplayer;

import me.zhengken.lyricview.LyricView;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.global.resource.ResourceManager;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertNotNull;

public class LyricviewTest {

    @Test
    public void testLyricView(){
        LyricView lyricView = new LyricView(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext());
        assertNotNull(lyricView);
    }

    @Test
    public void testLyricSetFile() throws IOException {
        LyricView lyricView = new LyricView(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext());
        ResourceManager resourceManager = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext().getResourceManager();
        InputStream mLrcInputStream = resourceManager.getRawFileEntry("resources/rawfile/qqfg.lrc").openRawFile();
        lyricView.setLyrcFileInputStream(mLrcInputStream);
    }
}