package me.zhengken.zkmusicplayer;


import me.zhengken.zkmusicplayer.eventbus.MediaPlayerCreatedEvent;
import me.zhengken.zkmusicplayer.eventbus.MusicControlEvent;
import me.zhengken.zkmusicplayer.eventbus.PlayServiceCreatedEvent;
import me.zhengken.zkmusicplayer.eventbus.UpdateUiEvent;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.common.Source;
import ohos.media.player.Player;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;



public class MusicPlayService extends Ability implements Player.IPlayerCallback{

    private HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"MusicPlayService");
    public static final int MUSIC_CONTROL_PLAY = 0;

    public static final int MUSIC_CONTROL_PAUSE = 1;

    private Player mMediaPlayer;

    private State mCurrentState;

    private Source mSongSource;

    @Override
    protected void onStop() {
        super.onStop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        mMediaPlayer = new Player(getContext());
        mMediaPlayer.setPlayerCallback(this);
        EventBus.getDefault().register(this);
        EventBus.getDefault().post(new MediaPlayerCreatedEvent(mMediaPlayer));
        EventBus.getDefault().post(new PlayServiceCreatedEvent());
    }

    @Override
    protected void onCommand(Intent intent, boolean restart, int startId) {
        super.onCommand(intent, restart, startId);
    }



    private void initMediaPlayer() {
        if (mSongSource == null){
            throw new NullPointerException();
        }
        mMediaPlayer.reset();
        setCurrentState(State.STATE_IDLE);
        mMediaPlayer.setSource(mSongSource);
        setCurrentState(State.START_INITIALIZED);
        mMediaPlayer.prepare();
    }

    @Subscribe
    public void onMusicControlEvent(MusicControlEvent event) {
        switch (event.mCtrlId) {
            case MUSIC_CONTROL_PLAY:
                // resume music
                if (mSongSource != null && mSongSource.equals(event.mSongSource)) {
                    play();
                    return;
                }

                mSongSource = event.mSongSource;
                initMediaPlayer();
                break;
            case MUSIC_CONTROL_PAUSE:
                pause();
                break;
        }
    }

    private void play() {
        if (mMediaPlayer != null && (mCurrentState == State.STATE_PAUSE || mCurrentState == State.STATE_PREPARED || mCurrentState == State.STATE_PLAYING || mCurrentState == State.STATE_COMPLETE)) {
            mMediaPlayer.play();
            setCurrentState(State.STATE_PLAYING);
            EventBus.getDefault().post(new UpdateUiEvent());
        }
    }

    private void pause() {
        if (mMediaPlayer != null && (mCurrentState == State.STATE_PLAYING || mCurrentState == State.STATE_PAUSE || mCurrentState == State.STATE_COMPLETE)) {
            mMediaPlayer.pause();
            setCurrentState(State.STATE_PAUSE);
        }
    }

    private void setCurrentState(State currentState) {
        mCurrentState = currentState;
    }

    @Override
    public void onPrepared() {
        setCurrentState(State.STATE_PREPARED);
        play();
    }

    @Override
    public void onMessage(int i, int i1) {
        HiLog.error(logLabel,"-------onMessage--------------");
    }

    @Override
    public void onError(int i, int i1) {
        HiLog.error(logLabel,"-------onError--------------");
    }

    @Override
    public void onResolutionChanged(int i, int i1) {
        HiLog.error(logLabel,"-------onResolutionChanged--------------");
    }

    @Override
    public void onPlayBackComplete() {
        HiLog.error(logLabel,"-------onPlayBackComplete---------%{public}d-----",mMediaPlayer.getCurrentTime());
    }

    @Override
    public void onRewindToComplete() {
        HiLog.error(logLabel,"-------onRewindToComplete--------------");
    }

    @Override
    public void onBufferingChange(int i) {

    }

    @Override
    public void onNewTimedMetaData(Player.MediaTimedMetaData mediaTimedMetaData) {

    }

    @Override
    public void onMediaTimeIncontinuity(Player.MediaTimeInfo mediaTimeInfo) {

    }

    private enum State {
        STATE_STOPPED, STATE_PREPARED, STATE_PLAYING, STATE_PAUSE, STATE_IDLE, STATE_END, STATE_ERROR, STATE_COMPLETE, START_INITIALIZED
    }
}
