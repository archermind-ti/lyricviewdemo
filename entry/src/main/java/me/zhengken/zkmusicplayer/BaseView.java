package me.zhengken.zkmusicplayer;



public interface BaseView<T> {

    void setPresenter(T presenter);
}
