package me.zhengken.zkmusicplayer.eventbus;


import ohos.media.player.Player;


public class MediaPlayerCreatedEvent {

    public Player mMediaPlayer;

    public MediaPlayerCreatedEvent(Player mediaPlayer) {
        if (mediaPlayer == null){
            throw new NullPointerException();
        }
        mMediaPlayer = mediaPlayer;
    }
}
