package me.zhengken.zkmusicplayer.eventbus;

import ohos.media.common.Source;


public class MusicControlEvent {

    public static int MUSIC_CONTROL_PLAY = 0;

    public static int MUSIC_CONTROL_PAUSE = 1;

    public int mCtrlId;

    public Source mSongSource;

    public MusicControlEvent(int ctrlId, Source songSource) {
        mCtrlId = ctrlId;
        mSongSource = songSource;
    }

}
