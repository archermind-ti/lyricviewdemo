package me.zhengken.zkmusicplayer;

import me.zhengken.zkmusicplayer.mainactivity.MainFragment;
import me.zhengken.zkmusicplayer.mainactivity.MainPresenter;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.bundle.IBundleManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbility extends FractionAbility {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,0X00201,"MainAbility");

    private MainPresenter mPresenter;

    private Intent mPlayService;

    private MainFragment mainFragment;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        mainFragment = new MainFragment();
        getFractionManager().startFractionScheduler().add(ResourceTable.Id_contentFrame,mainFragment).submit();
        if (verifySelfPermission("ohos.permission.READ_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
            if (canRequestPermission("ohos.permission.READ_USER_STORAGE")) {
                requestPermissionsFromUser(
                        new String[] { "ohos.permission.READ_USER_STORAGE"} , 1);
            }
        }else{
            mPlayService = new Intent();

            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("me.zhengken.zkmusicplayer")
                    .withAbilityName("me.zhengken.zkmusicplayer.MusicPlayService")
                    .build();
            mPlayService.setOperation(operation);
            startAbility(mPlayService);
            mPresenter = new MainPresenter(mainFragment, this);
            mPresenter.processPath("resources/rawfile/qqfg.mp3");
        }

    }

    @Override
    public void onRequestPermissionsFromUserResult (int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    try {
                        mPlayService = new Intent();

                        Operation operation = new Intent.OperationBuilder()
                                .withDeviceId("")
                                .withBundleName("me.zhengken.zkmusicplayer")
                                .withAbilityName("me.zhengken.zkmusicplayer.MusicPlayService")
                                .build();
                        mPlayService.setOperation(operation);
                        startAbility(mPlayService);
                        mPresenter = new MainPresenter(mainFragment, this);
                        mPresenter.processPath("resources/rawfile/qqfg.mp3");
                    }catch (Exception e){
                        HiLog.error(label,"onRequestPermissionsFromUserResult Exception %{public}s",e.getMessage());
                    }
                }
                return;
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mPresenter.processPath(intent.getUri().getDecodedPath());
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopAbility(mPlayService);
        mPresenter.destroy();
    }
}
