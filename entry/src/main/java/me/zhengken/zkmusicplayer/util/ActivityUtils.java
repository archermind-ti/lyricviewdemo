/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.zhengken.zkmusicplayer.util;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;

/**
 * This provides methods to help Activities load their UI.
 */
public class ActivityUtils {

    public static void addFragmentToActivity (FractionManager fragmentManager,
                                              Fraction fragment, int frameId) {
        if(fragmentManager == null || fragment == null){
            throw new NullPointerException();
        }

        FractionScheduler transaction = fragmentManager.startFractionScheduler();
        transaction.add(frameId, fragment);
        transaction.submit();
    }

}
