package me.zhengken.zkmusicplayer.util;



import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public class ToastUtils {

    // 短时间显示Toast信息
    public static void showShort(Context context, String info) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setText(info).setAlignment(LayoutAlignment.HORIZONTAL_CENTER | LayoutAlignment.VERTICAL_CENTER).setOffset(0,450).setDuration(500).show();
    }

    // 长时间显示Toast信息
    public static void showLong(Context context, String info) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setText(info).setAlignment(LayoutAlignment.HORIZONTAL_CENTER | LayoutAlignment.VERTICAL_CENTER).setOffset(0,450).setDuration(1000).show();
    }

}
