package me.zhengken.zkmusicplayer.util;

import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;


public class ImageUtils {

    /**
     * 为音乐播放器定制的倒影图片计算函数
     * 在前端图片倒影 ImageView 的大小为 568 * 100 = 5.68 ：1 (px)
     * 所以应该按照 w : h = 5.68 : 1 的比例对原图片进行倒影，这样放在前端 ImageView 中才不会变形
     * 又因为倒影计算过程中的 w 是不变的，所以 h = w / 5.68
     * 那么计算倒影所对应的坐标为 Rect(0, h - w / 5.68, w, w / 5.68)
     *
     * @param src
     * @return
     */
    public static PixelMap createReflectionBitmapForSingle(PixelMap src, int width, int height) {
        final int w = src.getImageInfo().size.width;
        final int h = src.getImageInfo().size.height;
        // 绘制高质量32位图

        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        Size size = new Size();
        size.width = w;
        size.height = height * h / width;
        options.size = size;
        options.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap bitmap = PixelMap.create(options);
        // 创建沿X轴的倒影图像
        Matrix m = new Matrix();
        m.setScale(1, -1);
        Rect rect = new Rect();
        rect.minX = 0;
        rect.minY = h - (height * w / width);
        rect.width = w;
        rect.height = height * w / width;

        PixelMap t_bitmap =  PixelMap.create(src,rect,options);

        Canvas canvas = new Canvas(new Texture(bitmap));
        Paint paint = new Paint();
        // 绘制倒影图像
        canvas.drawPixelMapHolder(new PixelMapHolder(t_bitmap),0,0,paint);
        // 线性渲染-沿Y轴高到低渲染
        Point[] newPoints = {new Point(0f,0f),new Point(0f,height * h / (float)width)};
        Color[] colors = {new Color(0x70ffffff),new Color(0x00ffffff)};
        Shader shader = new LinearShader(newPoints,null,colors, Shader.TileMode.MIRROR_TILEMODE);

        paint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
        // 取两层绘制交集。显示下层。
        paint.setBlendMode(BlendMode.DST_IN);
        // 绘制渲染倒影的矩形
        canvas.drawRect(0, 0, w, height * h / (float)width, paint);
        return bitmap;
    }

    /**
     * 模糊图片的具体方法
     *
     * @param context 上下文对象
     * @param image   需要模糊的图片
     * @param scale   图片缩放比例
     * @param radius  最大模糊度(在0.0到25.0之间)
     * @return 模糊处理后的图片
     */

    public static PixelMap blur(Context context, PixelMap image, float scale, float radius) {
        // 计算图片缩小后的长宽
        int width = Math.round(image.getImageInfo().size.width * scale);
        int height = Math.round(image.getImageInfo().size.height * scale);

        // 将缩小后的图片做为预渲染的图片。
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size.width = width;
        options.size.height = height;

        PixelMap inputBitmap = PixelMap.create(image,options);
        // 创建一张渲染后的输出图片。
        return PixelMap.create(inputBitmap,options);
    }

    /**
     * Stack Blur v1.0 from
     * http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
     * Java Author: Mario Klingemann <mario at quasimondo.com>
     * http://incubator.quasimondo.com
     * <p>
     * created Feburary 29, 2004
     * Android port : Yahel Bouaziz <yahel at kayenko.com>
     * http://www.kayenko.com
     * ported april 5th, 2012
     * <p>
     * This is a compromise between Gaussian Blur and Box blur
     * It creates much better looking blurs than Box Blur, but is
     * 7x faster than my Gaussian Blur implementation.
     * <p>
     * I called it Stack Blur because this describes best how this
     * filter works internally: it creates a kind of moving stack
     * of colors whilst scanning through the image. Thereby it
     * just has to add one new block of color to the right side
     * of the stack and remove the leftmost color. The remaining
     * colors on the topmost layer of the stack are either added on
     * or reduced by one, depending on if they are on the right or
     * on the left side of the stack.
     * <p>
     * If you are using this algorithm in your code please add
     * the following line:
     * Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>
     */

    public static PixelMap fastblur(PixelMap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getImageInfo().size.width * scale);
        int height = Math.round(sentBitmap.getImageInfo().size.height * scale);

        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        Size size = new Size();
        size.width = width;
        size.height = height;
        options.size = size;
        options.editable = true;
        PixelMap tmpBitmap;
        tmpBitmap = PixelMap.create(sentBitmap,options);

        PixelMap bitmap = PixelMap.create(tmpBitmap,options);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getImageInfo().size.width;
        int h = bitmap.getImageInfo().size.width;

        int[] pix = new int[w * h];
        Rect rect = new Rect();
        rect.minX = 0;
        rect.minY = 0;
        rect.width = w;
        rect.height = h;
        bitmap.readPixels(pix,0,w,rect);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.writePixels(pix,0,w,rect);
        return (bitmap);
    }


}
