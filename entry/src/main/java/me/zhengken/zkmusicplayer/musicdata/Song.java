package me.zhengken.zkmusicplayer.musicdata;


import me.zhengken.zkmusicplayer.MyApplication;
import me.zhengken.zkmusicplayer.ResourceTable;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileDescriptor;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.common.Source;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVMetadataHelper;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;




public class Song {
    private HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"Song");

    public Song(Uri songUri,Context context) {
        if (songUri == null || context == null){
            throw new NullPointerException();
        }
        try {
            String songPath = URLDecoder.decode(songUri.getDecodedPath(), "UTF8");
            mSongPath = songPath;
            mContext = context;
        } catch (UnsupportedEncodingException e) {
            HiLog.error(logLabel,"Song(Uri songUri) UnsupportedEncodingException %{public}s",e.getMessage());
        }

    }

    public Song(String songPath,Context context) {
        if (songPath == null || context == null){
            throw new NullPointerException();
        }
        mSongPath = songPath;
        mContext = context;
    }

    private String mTitle;

    private String mArtist;

    private String mLrcPath;

    private Context mContext;

    private String mSongPath;

    private int mDuration;

    private PixelMap mCover;

    public String getmSongPath() {
        return mSongPath;
    }

    private RawFileDescriptor mRawFileDescriptor;
    private InputStream mLrcInputStream;
    private Source mSongSource;

    private void setLrcPath() {
        if (mSongPath == null){
            throw new NullPointerException();
        }
        String lrcPath = mSongPath.substring(0, mSongPath.length() - 3) + "lrc";
        mLrcPath = lrcPath;
    }

    public String getLrcPath() {
        if (mLrcPath == null) {
            setLrcPath();
        }

        if (isLrcExist()) {
            return mLrcPath;
        }
        return null;
    }

    public boolean isLrcExist() {
        if (mLrcPath == null){
            throw new NullPointerException();
        }

        File lrcFile = new File(mLrcPath);
        if (lrcFile.exists()) {
            return true;
        }
        return false;
    }

    public RawFileDescriptor getSongFileDescriptor() {
        try {
            if (mRawFileDescriptor == null){
                ResourceManager resourceManager = mContext.getResourceManager();
                mRawFileDescriptor = resourceManager.getRawFileEntry("resources/rawfile/qqfg.mp3").openRawFileDescriptor();
            }
            return mRawFileDescriptor;
        }catch (IOException e){
            return null;
        }

    }
    public Source getSongSource() {
            if (mSongSource == null){
                mSongSource = new Source(getSongFileDescriptor().getFileDescriptor(),getSongFileDescriptor().getStartPosition(),getSongFileDescriptor().getFileSize());
            }
            return mSongSource;
    }

    public InputStream getLrcInputStream(){
        try {
            if (mLrcInputStream == null){
                ResourceManager resourceManager = mContext.getResourceManager();
                mLrcInputStream = resourceManager.getRawFileEntry("resources/rawfile/qqfg.lrc").openRawFile();
            }
            return mLrcInputStream;
        }catch (IOException e){
            return null;
        }

    }
    public void resetLrcInputStream(){
        if (mLrcInputStream != null){
            try {
                mLrcInputStream.close();
                mLrcInputStream = null;
            } catch (IOException e) {
                HiLog.error(logLabel,"resetLrcInputStream IOException %{public}s",e.getMessage());
            }

        }
    }
    public void setmTitle() {
        AVMetadataHelper avMetadataHelper = new AVMetadataHelper();
        avMetadataHelper.setSource(getSongFileDescriptor().getFileDescriptor(),getSongFileDescriptor().getStartPosition(),getSongFileDescriptor().getFileSize());
        mTitle = avMetadataHelper.resolveMetadata(AVMetadataHelper.AV_KEY_TITLE);
    }

    public String getmTitle() {
        if (mTitle == null)
            setmTitle();
        return mTitle;
    }

    private void setArtist() {
        AVMetadataHelper avMetadataHelper = new AVMetadataHelper();
        avMetadataHelper.setSource(getSongFileDescriptor().getFileDescriptor(),getSongFileDescriptor().getStartPosition(),getSongFileDescriptor().getFileSize());
        mArtist = avMetadataHelper.resolveMetadata(AVMetadataHelper.AV_KEY_ARTIST);
    }

    public String getArtist() {
        if (mArtist == null) {
            setArtist();
        }
        return mArtist;
    }

    public void setDuration() {
        AVMetadataHelper avMetadataHelper = new AVMetadataHelper();
        avMetadataHelper.setSource(getSongFileDescriptor().getFileDescriptor(),getSongFileDescriptor().getStartPosition(),getSongFileDescriptor().getFileSize());
        mDuration = Integer.valueOf(avMetadataHelper.resolveMetadata(AVMetadataHelper.AV_KEY_DURATION));
    }

    public int getDuration() {
        if (mDuration == 0) {
            setDuration();
        }
        return mDuration;
    }

    public String getFormatDuration() {
        long time = getDuration();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(time));
        double minute = calendar.get(Calendar.MINUTE);
        double second = calendar.get(Calendar.SECOND);

        DecimalFormat format = new DecimalFormat("00");
        return format.format(minute) + ":" + format.format(second);
    }


    public void setCover() {
        AVMetadataHelper avMetadataHelper = new AVMetadataHelper();
        avMetadataHelper.setSource(getSongFileDescriptor().getFileDescriptor(),getSongFileDescriptor().getStartPosition(),getSongFileDescriptor().getFileSize());
        byte[] bitmap = avMetadataHelper.resolveImage();

        if (bitmap != null) {
            ImageSource imageSource = ImageSource.create(bitmap,0,bitmap.length,null);
            mCover = imageSource.createPixelmap(null);
        } else {
            try {
                String path = mContext.getResourceManager().getMediaPath(ResourceTable.Media_default_cover);
                ImageSource imageSource = ImageSource.create(path,null);
                mCover = imageSource.createPixelmap(null);
            } catch (IOException e) {
                HiLog.error(logLabel,"setCover IOException %{public}s",e.getMessage());
            } catch (NotExistException e) {
                HiLog.error(logLabel,"setCover NotExistException %{public}s",e.getMessage());
            } catch (WrongTypeException e) {
                HiLog.error(logLabel,"setCover WrongTypeException %{public}s",e.getMessage());
            }
        }
    }

    public PixelMap getCover() {
        if (mCover == null) {
            setCover();
        }
        return mCover;
    }

    public void setCoverMirror() {
    }

    public void setCoverGauss() {
    }
}
