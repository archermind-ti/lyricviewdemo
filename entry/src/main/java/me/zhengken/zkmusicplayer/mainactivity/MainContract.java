package me.zhengken.zkmusicplayer.mainactivity;

import me.zhengken.zkmusicplayer.BasePresenter;
import me.zhengken.zkmusicplayer.BaseView;
import me.zhengken.zkmusicplayer.musicdata.Song;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.InputStream;


interface MainContract {
    interface View extends BaseView<Presenter> {

        void updateTitle(String title);

        void updateArtist(String artist);

        void setEndTime(String time);

        void resetSeekBar(int max);

        void setListener();

        void initLrcView(File lrcFile);

        void initLrcViewByStream(InputStream fis);

        void updateLrcView(int progress);

        void updateCoverGauss(PixelMap bitmap);

        void updateCover(PixelMap bitmap);

        void updateCoverMirror(PixelMap bitmap);

        /**
         * 设置 播放-暂停 按钮图片
         *
         * @param isPlaying -true: play图片 -false: pause图片
         */
        void updatePlayButton(boolean isPlaying);

        void updateSeekBar(int progress);

    }

    interface Presenter extends BasePresenter {

        void processPath(String filePath);

        void pause();

        void play();

        void playMusic(Song song);

        void onBtnPlayPausePressed();

        void onProgressChanged(int progress);

        void destroy();

    }
}
