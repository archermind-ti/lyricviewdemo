package me.zhengken.zkmusicplayer.mainactivity;

import me.zhengken.zkmusicplayer.eventbus.MediaPlayerCreatedEvent;
import me.zhengken.zkmusicplayer.eventbus.PlayServiceCreatedEvent;
import me.zhengken.zkmusicplayer.eventbus.UpdateUiEvent;
import me.zhengken.zkmusicplayer.musicdata.PlayList;
import me.zhengken.zkmusicplayer.musicdata.Song;
import me.zhengken.zkmusicplayer.util.ImageUtils;
import me.zhengken.zkmusicplayer.util.TextUtils;
import me.zhengken.zkmusicplayer.util.ToastUtils;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.player.Player;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;


public class MainPresenter implements MainContract.Presenter {
    private HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"MainPresenter");

    private static final int MSG_SEEK_BAR_REFRESH = 0;
    private static final int MSG_MUSIC_LRC_REFRESH = 1;

    private MainContract.View mMainView;

    private PlayList mPlayList;

    private Player mMediaPlayer;

    private Context mContext;

    public MainPresenter(MainContract.View mainView, Context context) {
        if (mainView == null){
            throw new NullPointerException("mainView cannot be null");
        }
        mMainView = mainView;
        mContext = context;
        mainView.setPresenter(this);

        mPlayList = PlayList.getmInstance();
        EventBus.getDefault().register(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void processPath(String filePath) {
        Song song = new Song(filePath,mContext);
        mPlayList.setIsThirdCall(true);
        mPlayList.setThirdSong(song);
        playMusic(mPlayList.getCurrSong());
    }

    @Override
    public void pause() {
        if (mMediaPlayer != null) {
            mPlayList.pause();
        }
    }

    @Override
    public void play() {
        handler.removeEvent(MSG_MUSIC_LRC_REFRESH);
        if (mMediaPlayer != null) {
            handler.sendEvent(MSG_MUSIC_LRC_REFRESH);
            mPlayList.play();
            mMainView.updatePlayButton(false);
        }
    }

    @Override
    public void destroy() {
        mMainView = null;
        handler.removeEvent(MSG_SEEK_BAR_REFRESH);
        handler.removeEvent(MSG_MUSIC_LRC_REFRESH);
        EventBus.getDefault().unregister(this);
    }

    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case MSG_SEEK_BAR_REFRESH:
                    mMainView.updateSeekBar(mMediaPlayer.getCurrentTime());
                    sendEvent(MSG_SEEK_BAR_REFRESH,1000);
                    break;
                case MSG_MUSIC_LRC_REFRESH:
                    if (mMediaPlayer != null) {
                        mMainView.updateLrcView(mMediaPlayer.getCurrentTime());
                    }
                    sendEvent(MSG_MUSIC_LRC_REFRESH, 120);
                    break;
            }
        }
    };

    @Override
    public void playMusic(Song song) {
        if (mMediaPlayer == null) {
            return;
        }
        try {
            mMainView.resetSeekBar(song.getDuration());
            mMainView.updateTitle(song.getmTitle());
            mMainView.updateArtist(song.getArtist());
            mMainView.setEndTime(TextUtils.duration2String(song.getDuration()));
            mMainView.updatePlayButton(false);
        }catch (Exception e){
            HiLog.error(logLabel,"--------exception-mMainView--%{public}s---",e.getMessage());
        }

        mPlayList.play();
        handler.sendEvent(MSG_SEEK_BAR_REFRESH);
    }

    @Override
    public void onBtnPlayPausePressed() {
        if (mMediaPlayer == null || mPlayList.getCurrSong() == null) {
            ToastUtils.showShort(mContext, "please open a local file");
            return;
        }


        boolean isPlaying = mMediaPlayer.isNowPlaying();
        if (isPlaying) {
            // avoid ui stuck when quick switch play and pause
            handler.removeEvent(MSG_MUSIC_LRC_REFRESH);
            //pause
            mPlayList.pause();
        } else {
            handler.sendEvent(MSG_MUSIC_LRC_REFRESH);
            //play
            mPlayList.play();
        }
        mMainView.updatePlayButton(isPlaying);
    }

    @Override
    public void onProgressChanged(int progress) {
        if (mMediaPlayer != null) {
            mMediaPlayer.rewindTo(progress*1000);
            mMainView.updateSeekBar(progress);
        }
    }

    @Subscribe
    public void onPlayServiceCreated(PlayServiceCreatedEvent event) {
        playMusic(mPlayList.getCurrSong());
    }

    @Subscribe
    public void onMediaPlayerCreated(MediaPlayerCreatedEvent event) {
        if(event == null){
            throw new NullPointerException();
        }
        mMediaPlayer = event.mMediaPlayer;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateUI(UpdateUiEvent event) {
        Song song = mPlayList.getCurrSong();

        if (song == null) {
            return;
        }
        PixelMap bitmap = song.getCover();
        if (bitmap != null) {
            mMainView.updateCoverGauss(ImageUtils.fastblur(bitmap, 0.1f, 10));
            mMainView.updateCover(bitmap);
            mMainView.updateCoverMirror(ImageUtils.createReflectionBitmapForSingle(bitmap,
                    220,
                    50));
        } else {
            mMainView.updateCoverGauss(null);
            mMainView.updateCover(null);
            mMainView.updateCoverMirror(null);
        }

        if (song.getLrcPath() != null) {
            mMainView.initLrcView(new File(song.getLrcPath()));
            handler.sendEvent(MSG_MUSIC_LRC_REFRESH);
        }else if(song.getLrcInputStream() != null){
          mMainView.initLrcViewByStream(song.getLrcInputStream());
          song.resetLrcInputStream();
          handler.sendEvent(MSG_MUSIC_LRC_REFRESH);
        } else {
            mMainView.initLrcView(null);
            handler.removeEvent(MSG_MUSIC_LRC_REFRESH);
        }
    }
}
