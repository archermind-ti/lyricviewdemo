package me.zhengken.zkmusicplayer.mainactivity;


import butterknife.BindComponent;
import butterknife.ButterKnife;
import me.zhengken.lyricview.LyricView;
import me.zhengken.zkmusicplayer.ResourceTable;
import me.zhengken.zkmusicplayer.util.TextUtils;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

import java.io.File;
import java.io.InputStream;


public class MainFragment extends Fraction implements MainContract.View, Component.TouchEventListener, Slider.ValueChangedListener, LyricView.OnPlayerClickListener {

    private MainContract.Presenter mPresenter;

    private boolean displayLrc = false;

    @BindComponent(ResourceTable.Id_background_blur)
    public Image mCoverGauss;

    @BindComponent(ResourceTable.Id_linear_layout_music_cover)
    public DirectionalLayout mDisplayLrc;

    @BindComponent(ResourceTable.Id_custom_lyric_view)
    public LyricView mLyricView;

    @BindComponent(ResourceTable.Id_cover)
    public Image mCover;

    @BindComponent(ResourceTable.Id_cover_mirror)
    public Image mCoverMirror;

    @BindComponent(ResourceTable.Id_music_title)
    public Text mTitle;

    @BindComponent(ResourceTable.Id_music_artist)
    public Text mArtist;

    @BindComponent(ResourceTable.Id_end_time)
    public Text mEndTime;

    @BindComponent(ResourceTable.Id_start_time)
    public Text mStartTime;

    @BindComponent(ResourceTable.Id_music_seek_bar)
    public Slider mSeekBar;

    @BindComponent(ResourceTable.Id_btn_mode)
    public Image mPlayMode;

    @BindComponent(ResourceTable.Id_btn_prev)
    public Image mPrev;

    @BindComponent(ResourceTable.Id_btn_play_pause)
    public Image mPlayPause;

    @BindComponent(ResourceTable.Id_btn_next)
    public Image mNext;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of this fragment using the provided
     * parameters.
     *
     * @return A new instance of fragment MainFragment.
     */
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        if (presenter == null){
            throw new NullPointerException();
        }
        mPresenter = presenter;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fragment_main,container,false);
        ButterKnife.bind(this,component);
        return  component;
    }

    @Override
    protected void onActive() {
        super.onActive();
        setListener();
    }


    @Override
    public void updateTitle(String title) {
        if (mTitle == null) {
            return;
        }
        mTitle.setText(title);
    }

    @Override
    public void updateArtist(String artist) {
        if (mArtist == null) {
            return;
        }
        mArtist.setText(artist);
    }

    @Override
    public void setEndTime(String time) {
        if (mEndTime == null) {
            return;
        }
        mEndTime.setText(time);
    }

    @Override
    public void resetSeekBar(int max) {
        if (mSeekBar == null) {
            return;
        }
        mSeekBar.setProgressValue(0);
        mSeekBar.setMaxValue(max);
    }


    @Override
    public void setListener() {
        mDisplayLrc.setTouchEventListener(this);
        mPlayMode.setTouchEventListener(this);
        mPrev.setTouchEventListener(this);
        mPlayPause.setTouchEventListener(this);
        mNext.setTouchEventListener(this);
        mSeekBar.setValueChangedListener(this);
        mLyricView.setOnPlayerClickListener(this);
    }



    @Override
    public void initLrcView(File lrcFile) {
        mLyricView.setLyricFile(lrcFile);
    }

    @Override
    public void initLrcViewByStream(InputStream fis) {
        mLyricView.setLyrcFileInputStream(fis);
    }


    @Override
    public void updateLrcView(int progress) {
        mLyricView.setCurrentTimeMillis(progress);
    }

    @Override
    public void updateCoverGauss(PixelMap bitmap) {
        if (bitmap != null) {
            mCoverGauss.setPixelMap(bitmap);
        } else {
            mCoverGauss.setPixelMap(ResourceTable.Media_default_cover_blur);
        }
    }

    @Override
    public void updateCover(PixelMap bitmap) {
        if (bitmap != null) {
            mCover.setPixelMap(bitmap);
        } else {
            mCover.setPixelMap(ResourceTable.Media_default_cover);
        }
    }

    @Override
    public void updateCoverMirror(PixelMap bitmap) {
        if (bitmap != null) {
            mCoverMirror.setPixelMap(bitmap);
        } else {
            mCoverMirror.setPixelMap(ResourceTable.Media_default_cover_mirror);
        }
    }

    @Override
    public void updatePlayButton(boolean setPlayImage) {
        if (mPlayPause == null) {
            return;
        }
        if (setPlayImage) {
            mPlayPause.setImageAndDecodeBounds(ResourceTable.Media_play_normal);
        } else {
            mPlayPause.setImageAndDecodeBounds(ResourceTable.Media_pause_normal);
        }
    }

    @Override
    public void updateSeekBar(int progress) {
        if (mSeekBar == null || mStartTime == null) {
            return;
        }
        mStartTime.setText(TextUtils.duration2String(progress));
        mSeekBar.setProgressValue(progress);
    }

    @Override
    public void onPlayerClicked(long progress, String content) {
        //mPresenter.play();
        mPresenter.onProgressChanged((int) progress);
    }

    @Override
    public void onProgressUpdated(Slider slider, int i, boolean b) {
        if (b) {
            mPresenter.onProgressChanged(i);
        }
    }

    @Override
    public void onTouchStart(Slider slider) {
        mPresenter.pause();
    }

    @Override
    public void onTouchEnd(Slider slider) {
        mPresenter.play();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if(touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN){
            switch (component.getId()) {
                case ResourceTable.Id_btn_play_pause:
                    mPresenter.onBtnPlayPausePressed();
                    break;
                case ResourceTable.Id_linear_layout_music_cover:
                    if (displayLrc = !displayLrc) {
                        mLyricView.setVisibility(Component.VISIBLE);
                        mCover.setVisibility(Component.HIDE);
                        mCoverMirror.setVisibility(Component.HIDE);
                    } else {
                        mLyricView.setVisibility(Component.HIDE);
                        mCover.setVisibility(Component.VISIBLE);
                        mCoverMirror.setVisibility(Component.VISIBLE);
                    }
                    break;
            }
            return true;
        }
        return false;
    }
}
