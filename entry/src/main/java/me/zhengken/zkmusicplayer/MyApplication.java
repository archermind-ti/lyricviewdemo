package me.zhengken.zkmusicplayer;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;
import org.greenrobot.eventbus.EventBus;

public class MyApplication extends AbilityPackage {

    @Override
    public void onInitialize() {
        super.onInitialize();
        EventBus.initThreadForHos(getUITaskDispatcher());
    }
}
