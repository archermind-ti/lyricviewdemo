V1.0.0
移植基本完成，支持功能如下：
1、主要支持xml属性中：fadeInFadeOut、hint、hintColor、textColor、highlightColor、textSize、lineSpace的设置
2、textAlign属性支持(0:最对齐;1:居中;2:右对齐)
暂不支持：
每行歌词的最大长度设置