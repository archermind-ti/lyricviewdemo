# LyricViewDemo

#### 项目介绍

- LyricView是一款强大灵活的定制化的歌词显示视图。

#### 安装教程
在模块下的build.gradle种添加以下信息：

```
​```
...
allprojects {
    repositories {
    	mavenCentral()
    }
}
...
dependencies {
	...
	implementation("com.gitee.archermind-ti:LyricViewDemo:1.0.0-beta")
	...
}
​```
```

### 截图

<img src="https://gitee.com/archermind-ti/lyricviewdemo/raw/master/screenshot/Lyricview.gif" width=280/>

#### 使用说明

### XML code
    //step 1
        <me.zhengken.lyricview.LyricView
            ohos:id="$+id:custom_lyric_view"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:visibility="hide"
            app:fadeInFadeOut="true"
            app:textAlign="1"
            app:lineSpace="5vp"
			app:textSize="16fp"
            app:textColor="#ff33b5e5"
         />
### Java code
    //step 2
    LyricView mLyricView = (LyricView)findComponentById(ResourceTable.Id_custom_lyric_view);
    
    //step 3
    mLyricView.setLyricFile(lyricFile);
    
    //step 4, update LyricView every interval
    mLyricView.setCurrentTimeMillis(progress);
    
    //step 5, implement the interface when user drag lyrics and click the play icon
    mLyricView.setOnPlayerClickListener(new LyricView.OnPlayerClickListener() {
            @Override
            public void onPlayerClicked(long progress, String content) {
                
            }
        });

#### 原库差异说明
- 暂不支持每行最大长度的设置。

#### 版本迭代


- v1.0.0


#### 版权和许可信息
```
Copyright 2016 zhengken

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
